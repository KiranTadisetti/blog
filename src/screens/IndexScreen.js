import React, { useContext,useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, Button, TouchableOpacity } from 'react-native'
import { Context } from '../context/BlogContext'
import { Feather } from '@expo/vector-icons'

const IndexScreen = ({ navigation }) => {
    const { state, deleteBlogPost ,getBlogPost} = useContext(Context)

    useEffect(()=>{
        getBlogPost();

        const listener=navigation.addListener('didFocus',()=>{getBlogPost()});

        //returns only when the screen is removed from the stack
        return ()=>{
            listener.remove();//which helps in memory leak save
        }
    },[]);

    return (
        <View>
            <FlatList
                data={state}
                keyExtractor={blogpost => blogpost.id + ''}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity onPress={() =>
                            navigation.navigate('Show', { id: item.id })
                        }>
                            <View style={styles.row}>
                                <Text style={styles.title}> {item.title} - {item.id}</Text>
                                <TouchableOpacity onPress={() => {
                                    deleteBlogPost(item.id)
                                }}>
                                    <Feather style={styles.icon} name='trash' />
                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                    )
                }}

            />
        </View>
    );
};

IndexScreen.navigationOptions = ({navigation}) => {
    return ({
        headerRight: <TouchableOpacity onPress={()=>{navigation.navigate('Create')}}>
            <Feather size={30} style={{ marginRight: 10 }} name='plus' />
        </TouchableOpacity>
    });
}


const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 20,
        borderTopWidth: 1,
        borderColor: 'gray',
        paddingHorizontal: 10
    },
    title: {
        fontSize: 18
    },
    icon: {
        fontSize: 24
    }
});

export default IndexScreen;


